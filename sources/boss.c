#include "../headers/boss.h"

Boss* b_init(){
    Boss *b = malloc(sizeof(Boss));
    b->direction = 1;
    b->move_count = 0;
    b->isHitable = 0;
    b->hasSpawn = 0;
    b->isShooting = 0;
    b->hp = 5;
    b->mx = 5;
    b->my = 6;

    b->sprite = malloc(sizeof(SDL_Surface));
    b->sprite = IMG_Load(BOSS_SPRITE);
    if(!b->sprite){
        printf("Can't load boss sprite.\n");
    }

    b->proj = malloc(sizeof(Projectile));
    if(!b->proj){
        printf("Can't allocate memory for boss sprite");
    }

    b->pos.x = 0;
    if(b->sprite)
        b->pos.y = b->sprite->h + ENEMY_HEIGHT_0 - 30;

    return b;
}

void b_draw(Boss *b, SDL_Surface *app_window){
    SDL_BlitSurface(b->sprite, NULL, app_window, &b->pos);
    if(b->isShooting == 1){
        SDL_BlitSurface(b->proj->sprite, NULL, app_window, &b->proj->pos);
    }
}

void b_move(Boss *b){

    if(b->isHitable){
        if(++b->move_count % 3 == 0){
            b->move_count = 0;

            // 1/5 chance to switch direction
            if(rand() % 25 == 0) b->direction *= -1;

            // Checking boss is on screen
            if(b->pos.x + (b->direction * b->mx) <= 0){
                b->direction = 1;
            }
            if(b->pos.x + b->sprite->w + (b->direction * MX) >= SCREEN_WIDTH){
                b->direction = -1;
            }

            b->pos.x += b->direction * b->mx;
        }
    }
}

void b_free(Boss *b){
    if(b->proj != NULL) free(b->proj);
    if(b->sprite != NULL) free(b->sprite);
    free(b);
}

void b_shoot(Boss *b, int diff, Shield **s){
    if(b->sprite){
        if(b->isShooting == 0 && rand() % (100 / diff) == 1){
            b->isShooting = 1;
            b->proj = malloc(sizeof(Projectile));
            b->proj->sprite = malloc(sizeof(SDL_Surface));
            b->proj->sprite = IMG_Load(MISSILE_SPRITE);
            b->proj->pos.x = b->pos.x + b->sprite->w / 2;
            b->proj->pos.y = b->pos.y + b->sprite->h;
        }
        else if(b->isShooting == 1){
            b->proj->pos.y += b->my + diff;

            for(int i = 0; i < NB_SHIELDS; i++){
                if (b->proj->pos.y >= s[i]->pos.y && s[i]->hp > 0){
                    if (b->proj->pos.x >= s[i]->pos.x && b->proj->pos.x <= s[i]->pos.x + s[i]->sprite->w){
                        s[i]->hp--;
                        b->isShooting = 0;
                        free(b->proj);
                        i = NB_SHIELDS;
                    }
                }
            }

            if(b->proj->pos.y >= SCREEN_HEIGHT){
                b->isShooting = 0;
                free(b->proj);
                //b->proj = NULL;
            }
        }
    }
}

void b_destroy(Boss *b){
    // b->sprite = NULL;
    b->isHitable = 0;
    if(b->isShooting){
        free(b->proj);
        b->isShooting = 0;
    }
    b->isHitable = 0;
    b->sprite = NULL;
}

