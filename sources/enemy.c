#include "../headers/enemy.h"

void randomEnemyGenerator(Enemy *e, int i, int j){
    e->direction = 1;
    e->proj = malloc(sizeof(Projectile));
    e->move_count = 0;
    e->isHitable = 1;
    e->isShooting = 0;
    e->sprite = malloc(sizeof(SDL_Surface));
    if (e->sprite == NULL){
        printf("CANT ALLOCATE ENEMY SPRITE\n");
    }

    int rnd = rand() % 3;
    // // printf("rnd is %d\n", rnd);
    switch (rnd){
        case 0:
            // printf("Loading %s...\n\n", ENEMY_1_SPRITE);
            e->sprite = IMG_Load(ENEMY_1_SPRITE);
            // printf("done\n");
            break;

        case 1:
            // printf("Loading %s...\n\n", ENEMY_2_SPRITE);
            e->sprite = IMG_Load(ENEMY_2_SPRITE);
            // printf("done\n");
            break;

        case 2:
            // printf("Loading %s...\n\n", ENEMY_3_SPRITE);
            e->sprite = IMG_Load(ENEMY_3_SPRITE);
            // printf("done\n");
            break;

        default:
            e->sprite = IMG_Load(ENEMY_1_SPRITE);
            break;
    }

    if (e->sprite == NULL){
        printf("Can't load enemy\n");
        return NULL;
    }
    // printf("Created enemy %d\n", i);
    e->pos.x = i * 1.5 * ENEMY_WIDTH;
    e->pos.y = j;
}

Enemy **init_enemies(){
    Enemy **e = malloc(sizeof(Enemy *) * NB_ENEMIES);

    for (int i = 0, x = 0, y = ENEMY_HEIGHT_0; i < NB_ENEMIES; i++, x++){
        if (x == ENEMIES_PER_LINE){
            x = 0;
            y += ENEMY_ROW_MARGIN;
        }
        // printf("[%d]: %d, %d\n", i, x, y);
        e[i] = malloc(sizeof(Enemy));
        randomEnemyGenerator(e[i], x , y); // ENEMY_WIDTH + SCREEN_WIDTH / 2
    }
    printf("\n");
    return e;
}

void e_move(Enemy **e, int nb){
    for(int i = 0; i < nb; i++){
        e[i]->move_count++; // Slows down enemies
        if (e[i]->move_count % 3 == 0){
            e[i]->move_count = 0;
            if (e[i]->pos.x + (e[i]->direction * MX) <= 0 && e[i]->isHitable == 1){
                e_down(e, nb);
            }
            if (e[i]->pos.x + (e[i]->direction * MX) >= SCREEN_WIDTH && e[i]->isHitable == 1){
                e_down(e, nb);
            }

            e[i]->pos.x += e[i]->direction * MX;
        }
    }
}

void e_down(Enemy **e, int nb){
    for(int i = 0; i < nb; i++){
        e[i]->direction *= -1;
        e[i]->pos.y += 20;
    }
}

void e_shoot(Enemy **e, int difficulty, Shield **s){
    for(int i = 0; i < NB_ENEMIES; i++){
        // srand(time(0));
        if(e[i]->sprite){

            if(e[i]->isShooting == 0 && rand() % (750 / difficulty) == 1){
                e[i]->isShooting = 1;
                e[i]->proj = malloc(sizeof(Projectile));
                e[i]->proj->sprite = malloc(sizeof(SDL_Surface));
                e[i]->proj->sprite = IMG_Load(MISSILE_SPRITE);
                e[i]->proj->pos.x = e[i]->pos.x + e[i]->sprite->w / 2;
                e[i]->proj->pos.y = e[i]->pos.y + e[i]->sprite->h;
            }
            else if(e[i]->isShooting == 1){
                e[i]->proj->pos.y += MY / 3 + difficulty;

                // Out of screen
                if(e[i]->proj->pos.y >= SCREEN_HEIGHT){

                    e[i]->isShooting = 0;
                    free(e[i]->proj);
                }

                // Hits shield
                for(int j = 0; j < NB_SHIELDS; j++){
                    if(e[i]->proj->pos.y >= s[j]->pos.y && s[j]->hp > 0){
                        if(e[i]->proj->pos.x >= s[j]->pos.x && e[i]->proj->pos.x <= s[j]->pos.x + s[j]->sprite->w){
                            s[j]->hp--;
                            e[i]->isShooting = 0;
                            free(e[i]->proj);
                            j = NB_SHIELDS;
                        }
                    }
                }
            }
        }
    }
}

void e_draw(Enemy **e, SDL_Surface *app_window, int nb){
    for(int i = 0; i < nb; i++){
        SDL_BlitSurface(e[i]->sprite, NULL, app_window, &e[i]->pos);
        if(e[i]->isShooting == 1){
            SDL_BlitSurface(e[i]->proj->sprite, NULL, app_window, &e[i]->proj->pos);
        }
    }
}

void e_free(Enemy **e, int nb){
    for(int i = 0; i < nb; i++){
        free(e[i]);
    }
}

void e_destroy(Enemy *e, int *e_remaining){
    e->sprite = NULL;
    e->isHitable = 0;
    *e_remaining--;
    if(e->isShooting){
        free(e->proj);
        e->isShooting = 0;
    }
}

int e_count(Enemy **e){
    int count = 0;
    for(int i = 0; i < NB_ENEMIES; i++){
        if(e[i]->sprite != NULL) count++;
    }
    return count;
}