    
#include "../headers/game.h"

/**
* 
* Game engine's brain
* returns the error code to the main function
*/
int process(SDL_Surface *app_window, Settings *settings){
    int error_code = 1; // 1 means everything was nice and juicy
    game_start(app_window, &error_code, settings);
    if(error_code != 1)
        return error_code;
    return 0;
}

/**
 * 
 * Initialize the game environments
 */
void game_start(SDL_Surface *app_window, int *error_code, Settings *settings){


    Game *game = malloc(sizeof(Game)); // Must be freed with game_quit()
    game->app_window = app_window;
    game->enemies_remaining = NB_ENEMIES;
    game->font = TTF_OpenFont(MAIN_POLICE, 10);
    game->settings = settings;

    // Checking if Game struct is ready to go
    if(!(game->app_window && game->enemies_remaining && game->font && game->settings)) *error_code = 1;

    *error_code == 1 ? game_loop(game, error_code) : game_quit(game, error_code);
}

void game_loop(Game *game, int *error_code){

    Mix_Music *laser = Mix_LoadMUS(LASER_SOUND);

    Player *p = p_init();
    Enemy **e = init_enemies();
    Shield **s = s_init();
    Boss *b = b_init();

    SDL_Event event;

    int loop = 1, shieldsUp = 1;
    SDL_EnableKeyRepeat(100, 2000);
    while(loop){
        
        SDL_PollEvent(&event);

        //Logic...
        switch(event.type){

            //Doesn't work here

            // case SDL_QUIT:
            //     loop = 0;
            //     break;

            case SDL_KEYDOWN: // Keyboard events
                switch(event.key.keysym.sym){
                    case SDLK_ESCAPE:
                        loop = 0;
                        *error_code = 0;
                        break;
                
                    case SDLK_q:
                        p_left(p);
                        break;
                    
                    case SDLK_d:
                        p_right(p);
                        break;

                    case SDLK_LEFT:
                        p_left(p);
                        break;

                    case SDLK_RIGHT:
                        p_right(p);
                        break;

                    case SDLK_SPACE:
                        // Mix_PlayMusic(laser, 0);
                        p_shoot(p, e, b, &game->enemies_remaining);
                        break;
                    
                    case SDLK_TAB:
                        game_pause(game, error_code, &event);
                        event.type = NULL;
                        break;

                    default:
                        break;
                }

            break; // End of keyboard handling

            default:
                break;
        }

        //Drawing...
        SDL_FillRect(game->app_window, NULL, SDL_MapRGB(game->app_window->format, 0, 0, 0));

        // Check for boss spawn
        if(e_count(e) <= NB_ENEMIES - (NB_ENEMIES / 10) && !b->hasSpawn){
            b->isHitable = 1;
            b->hasSpawn = 1;
        }

        // If player is hit
        for(int i = 0; i < NB_ENEMIES; i++){
            if(e[i]->sprite && e[i]->isShooting){
                if (e[i]->proj->pos.y + e[i]->proj->sprite->h >= p->pos.y &&
                    e[i]->proj->pos.y <= p->pos.y + p->sprite->h)
                {
                    if (e[i]->proj->pos.x + e[i]->proj->sprite->w >= p->pos.x &&
                        e[i]->proj->pos.x <= p->pos.x + p->sprite->w)
                    {
                        p->hp.amount--;
                        free(e[i]->proj);
                        e[i]->isShooting = 0;
                    }
                }
            }
        }

        if(b->isHitable == 1 && b->isShooting){
            if (b->proj->pos.y + b->proj->sprite->h >= p->pos.y && b->proj->pos.y <= p->pos.y + p->sprite->h){
                if (b->proj->pos.x + b->proj->sprite->w >= p->pos.x && b->proj->pos.x <= p->pos.x + p->sprite->w){
                    p->hp.amount--;
                    b->isShooting = 0;
                    free(b->proj);
                    b->proj = NULL;
                }
            }
        }

        // If enemies pass shields
        if(shieldsUp){
            for(int i = NB_ENEMIES - 1; i >= 0; i--){
                if(e[i]->sprite != NULL){
                    if(e[i]->sprite->h + e[i]->pos.y >= s[0]->pos.y){
                        shieldsUp = 0;
                    }
                }
            }
        }

        if(b->isHitable == 1){
            b_draw(b, game->app_window);
            b_shoot(b, game->settings->difficulty_current, s);
            b_move(b);
            if (b->hp == 0){
                b_destroy(b);
            }
        }

        e_move(e, NB_ENEMIES);
        e_shoot(e, game->settings->difficulty_current, s);
        p_blit(p, e, b, game->app_window, game->font, &game->enemies_remaining);
        e_draw(e, game->app_window, NB_ENEMIES);
        if(shieldsUp){
            s_blit(s, game->app_window);
        }
        SDL_Flip(game->app_window);
        
        if(p_isDead(p, e)){
            loop = win_loss_screen(game, 0);
            printf("Score: %d\n", p->score);
            //*error_code = 0;
            event.type = NULL;

            // Handle retry...
        }else if (e_count(e) == 0 && b->isHitable == 0){
            loop = win_loss_screen(game, 1);
            event.type = NULL;
        }

        // Because PollEvent() loops too fast,
        // We use usleep() to slow down the game
        usleep(GAME_SLEEP_TIME);
    }

    // SDL_EnableKeyRepeat(0, 0)
    p_free(p);
    e_free(e, NB_ENEMIES);
    free(laser);
    free(e);
    s_free(s);
    b_free(b);
    game_quit(game, error_code);
}

void game_pause(Game *game, int *error_code, SDL_Event *e){
    SDL_Color white = {255, 255, 255}, green = {0, 255, 0}, black = {0, 0, 0};
    //SDL_Event e;

    TTF_Font *font = TTF_OpenFont(MAIN_POLICE, 20);
    SDL_Surface *play_btn, *menu_btn, *title;
    
    SDL_Rect play_pos, menu_pos, title_pos;

    title = TTF_RenderText_Shaded(font, "PAUSE", green, black);
    play_btn = TTF_RenderText_Shaded(game->font, PLAY_BTN, white, black);
    menu_btn = TTF_RenderText_Shaded(game->font, "MENU", white, black);

    // Placing surfaces...
    title_pos.x = SCREEN_WIDTH / 2 - title->w / 2;
    title_pos.y = SCREEN_HEIGHT / 2 - title->h / 2;
    play_pos.x = SCREEN_WIDTH / 2 - play_btn->w / 2;
    play_pos.y = title_pos.y + 35;// + title->h;
    menu_pos.x = SCREEN_WIDTH / 2 - menu_btn->w / 2;
    menu_pos.y = play_pos.y + 25;// + play_btn->h;

    int loop = 1;
    while(loop){
        SDL_WaitEvent(e);
        switch(e->type){
            case SDL_KEYDOWN:
                switch(e->key.keysym.sym){
                    case SDLK_ESCAPE:
                        loop = 0;
                        break;
                    case SDLK_TAB:
                        loop = 0;
                        break;
                    default:
                        break;
                }
            break;

            case SDL_MOUSEMOTION:
                if(mouseHover(e->button.x, e->button.y, play_btn->w, play_btn->h, play_pos)){
                    play_btn = TTF_RenderText_Shaded(game->font, PLAY_BTN, green, black);
                }else{
                    play_btn = TTF_RenderText_Shaded(game->font, PLAY_BTN, white, black);
                }

                if(mouseHover(e->button.x, e->button.y, menu_btn->w, menu_btn->h, menu_pos)){
                    menu_btn = TTF_RenderText_Shaded(game->font, "MENU", green, black);
                }else{
                    menu_btn = TTF_RenderText_Shaded(game->font, "MENU", white, black);
                }
            break;

            case SDL_MOUSEBUTTONDOWN:
                if(e->button.button == SDL_BUTTON_LEFT){
                    if(mouseHover(e->button.x, e->button.y, play_btn->w, play_btn->h, play_pos)){
                        loop = 0;
                    }

                    if(mouseHover(e->button.x, e->button.y, menu_btn->w, menu_btn->h, menu_pos)){
                        loop = 0;
                        // Set game_loop()::loop = 0;
                    }
                }
            break;
        }   

        SDL_BlitSurface(title, NULL, game->app_window, &title_pos);
        SDL_BlitSurface(play_btn, NULL, game->app_window, &play_pos);
        SDL_BlitSurface(menu_btn, NULL, game->app_window, &menu_pos);
        SDL_Flip(game->app_window);
    }

    // Freeing memory...
    free(title);
    free(play_btn);
    free(menu_btn);
    free(font);
}

void game_quit(Game *game, int *error_code){

    // switch(game->error_code){

    //     default:
    //         break;
    // }

    /**
     * 
     * To be freed:
     * 
     * player.projectile
     * player.sprite
     * game
     */
    free(game);
}

int win_loss_screen(Game *game, int status){
    int loop = 1; 
    int return_status = 0; // Quit by default

    SDL_Color green = {0, 255, 0};
    SDL_Color red = {255, 0, 0};
    SDL_Color black = {0, 0, 0};
    SDL_Surface *win_loss_text;
    SDL_Rect pos;

    // Player won
    if (status)
        win_loss_text = TTF_RenderText_Shaded(game->font, "YOU WIN", green, black);
    // Player lost
    else
        win_loss_text = TTF_RenderText_Shaded(game->font, "YOU LOSE", red, black);

    pos.x = SCREEN_WIDTH / 2 - win_loss_text->w / 2;
    pos.y = SCREEN_HEIGHT / 2 - win_loss_text->h / 2;
    SDL_BlitSurface(win_loss_text, NULL, game->app_window, &pos);
    SDL_Flip(game->app_window);

    while (loop){
            
        SDL_Event e;
        SDL_WaitEvent(&e);
        if (e.type == SDL_KEYDOWN)
        switch (e.key.keysym.sym){
            // Quit
            case SDLK_ESCAPE:
                return_status = 0;
                loop = 0;
                break;
            
            // Replay
            case SDLK_r:
                return_status = 1;
                loop = 0;
                break;
            
            default:
                break;
        }
    }
    return return_status;
}