#ifndef SETTINGS_H_
#define SETTINGS_H_

    #include <SDL/SDL.h>
    #include <SDL/SDL_image.h>
    #include <SDL/SDL_audio.h>
    #include <SDL/SDL_ttf.h>
    #include <SDL/SDL_mixer.h>
    
    #include <stdio.h>
    #include <stdlib.h>

    // #define t1 "FULL SCREEN"
    // #define t2 "DISABLED"

    #define SCREEN_WIDTH        500
    #define SCREEN_HEIGHT       350

    #define LEFT_ARROW "<"
    #define RIGHT_ARROW ">"
    #define LEFT_ARROW_X SCREEN_WIDTH - 200
    #define RIGHT_ARROW_X SCREEN_WIDTH - 40

    // Game consts...
    #define MAIN_POLICE     "shared/fonts/pixeled.ttf"
    #define SECONDARY_POLICE "shared/fonts/script.ttf"
    #define GAME_SLEEP_TIME 10000

    #define ENEMY_WIDTH 8
    #define ENEMY_HEIGHT 8
    #define NB_ENEMIES 60
    #define ENEMIES_PER_LINE 12
    #define ENEMY_HEIGHT_0 50   // First row height
    #define ENEMY_ROW_MARGIN 20 // Height between two rows
    #define NB_SHIELDS 4

    // Paths to sprites
    #define PLAYER_SPRITE   "shared/images/ovni.png"
    #define MISSILE_SPRITE  "shared/images/proj.png"
    #define BOSS_SPRITE     "shared/images/boss.png"
    #define ENEMY_1_SPRITE  "shared/images/enemy_1.png"
    #define ENEMY_2_SPRITE  "shared/images/enemy_2.png"
    #define ENEMY_3_SPRITE  "shared/images/enemy_3.png"
    #define HP_FULL         "shared/images/hp_full.png"
    #define HP_HALF         "shared/images/hp_half.png"
    #define HP_EMPTY        "shared/images/hp_empty.png"
    #define SHIELD_FULL     "shared/images/shield_full.png"
    #define SHIELD_HALF     "shared/images/shield_half.png"

        // String consts...
    #define TITLE       "Space Invaders"
    #define PLAY_BTN    "PLAY"
    #define OPT_BTN     "SETTINGS"
    #define QUIT_BTN    "QUIT"

    // Path to sounds
    #define THEME_SOUND "shared/sounds/theme.mp3"
    #define LASER_SOUND "shared/sounds/laser0.mp3"

    #define MX 2
    #define MY 5

    #define CONFIG_FILE "config.ini"

    typedef struct settings{
        int fs_current;
        int audio_current;
        int difficulty_current;
    }Settings;

    int mouseHover(int mx, int my, int ow, int oh, SDL_Rect objectCoords);
    int centerText(int text_w, int ra_w);
    int init(); // Initialize /!\ SDL COMPONENTS /!\

    void settings_show(SDL_Surface *app_window, Settings *s);
    void settings_init(Settings *settings);
    void settings_save(Settings s);

#endif