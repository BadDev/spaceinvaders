#ifndef GAME_H_
#define GAME_H_

    #include <SDL/SDL.h>
    #include <SDL/SDL_ttf.h>
    #include <SDL/SDL_image.h>
    #include <SDL/SDL_mixer.h>
    // #include <SDL/SDL_audio.h>

    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>  

    #include "settings.h"
    #include "player.h"
    #include "enemy.h"
    #include "shield.h"
    #include "boss.h"

    /**
    * 
    * Creating a Game type 
    * storing what it needs
    */
    typedef struct game{

        int lvl_id;
        int enemies_remaining;

        SDL_Surface *app_window; // From main()
        
        Settings *settings;

        TTF_Font *font;

        // Player *player;

    }Game;

    /**
     * 
     * Game engine's brain
     * returns the error code to the main function
     */
    int process(SDL_Surface *app_window, Settings *settings);

    /**
     * 
     * Initialize the game environments
     * 
     * SDL_Surface* -> window created in main
     */
    void game_start(SDL_Surface *app_window, int *error_code, Settings *settings);

    /**
     * Where the magic happens
     * 
     * Game* -> created in game_start()
     */
    void game_loop(Game *game, int *error_code);

    /**
     * Pause window
     */
    void game_pause(Game *game, int *error_code, SDL_Event *e);

    /**
     * Free memory and gives control back to main
     */
    void game_quit(Game *game, int *error_code);

    int win_loss_screen(Game *game, int status);
#endif